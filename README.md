# Minimal CSS with good display

CSS file in [`minimal.css`](minimal.css).

[`index.html`](index.html) from <https://github.com/cbracco/html5-test-page>
with minor modifications: HTTPS for external links, add display of a SVG image
for `img` tag.


## References

Based and inspired by examples and discussion from:

- [339 of reponsive
  CSS](https://blog.koley.in/2019/339-bytes-of-responsive-css)
  (copy here: [references/339-bytes-style.css](references/339-bytes-style.css))

- [58 bytes of css to look great nearly everywhere](https://jrl.ninja/etc/1/),
  with also a look at [post.css](https://jrl.ninja/etc/post.css) from author
  web site (copy here: [post.css](references/post.css)). Discussed at lobster
  <https://lobste.rs/s/tdeve3>.

- [When fonts fall](https://www.figma.com/blog/when-fonts-fall/) about fonts
  selections failure in 2020.

- Inspects your font: <https://fontdrop.info>.
  Learn more about a font and its capabilities to have better typography in
  your web site and documents.

- [An ode to OpenType: Fall in love with the secret world of
  fonts](https://www.figma.com/blog/opentype-font-features)
